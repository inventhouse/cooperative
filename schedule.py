# Schedule: Copyright © 2021 Benjamin Holt - MIT License

from collections import namedtuple
import time


RepeatingTask = namedtuple("RepeatingTask", "task", "next_t", "repeat_s", defaults=[None, None])  # Cut for _min


class Schedule():
    def __init__(self, tasks=None, wait=time.sleep):
        self._tasks = list(tasks) if tasks is not None else []  # [(task, next_t, repeat_s), ...]
        self.wait = wait


    def __call__(self, seconds=-1):
        end_t = time.monotonic() + seconds if seconds >= 0 else None  # Compute timeout for do-while exit condition later
        while True:
            task, next_t, repeat_s = self._tasks[0] if self._tasks else (None, None, None)
            if next_t is not None and next_t <= time.monotonic():  # Task is ready, run and perhaps re-schedule
                result = task() if callable(task) else task
                if result is not None:
                    self._tasks.append((lambda: self.wait(0), time.monotonic(), None))  # Will return a result, ensure any sub-scheduler we may have advances anyway

                next_t = next_time(next_t, repeat_s)
                if next_t is not None:
                    self._tasks.append((task, next_t, repeat_s))

                self._tasks = sorted(self._tasks[1:], key=lambda i: i[1])

                if result is not None:
                    return result

            now = time.monotonic()  # Use a single consistent 'now' for calculating wait seconds
            end_s = max(end_t - now, 0) if end_t is not None else None

            if not self._tasks:  # Wait if there's nothing to do  # REM: should this wait forever?
                return self.wait(end_s if end_s is not None else seconds)

            _, next_t, _ = self._tasks[0]
            delta_s = max(next_t - now, 0)
            wait_s = min(delta_s, end_s) if end_s is not None else delta_s

            result = self.wait(wait_s)
            if result is not None:
                return result

            if end_t is not None and end_t <= time.monotonic():
                return None


    def __next__(self):  # Fold into __call__ for _min
        ...

    def __iadd__(self, task):  # Cut for _min
        ...

    def __isub__(self, task):  # Cut for _min
        # if task not in self.tasks:  # Be lenient about removing tasks that aren't scheduled
        #     return self
        ...

    def __len__(self):  # Cut for _min
        return len(self.tasks)


    def __iter__(self):  # Cut for _min
        return self
#####


###  Utils  ###
def next_time(t=None, repeat_s=None):  # REM: maybe fold-in for _min
    now = time.monotonic()
    if not t:
        return now
    s = repeat_s() if callable(repeat_s) else repeat_s  # BJH: REM: is dynamic repeat seconds a good idea?  Powerful, but adds complexity, maybe cut for _min
    if s is None:
        return None  # None means "don't re-schedule"
    if s <= 0:
        return now  # Repeat 0 means "schedule ASAP"
    if t > now:  # Strictly greater-than; if a task ran but monotonic hasn't advanced enough, we don't want to repeat it
        return t

    t += s
    while t < now:  # Catch up if we've missed a beat, but stay on-cadence
        s = repeat_s() if callable(repeat_s) else repeat_s
        if s is None:
            return None
        if s <= 0:
            return now
        t += s

    return t
#####
