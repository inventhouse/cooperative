Cooperative
===========
_A small modular cooperative multi-tasking system for Python_


To Do
-----

#### Docs
- DONE: license
- More complete description in README


#### Examples
- Tennis for CPX?
- Hunt the wumpus w/ realtime wumpus & bat movement?


#### Tests


#### Statemachine

_Event manager_

- DONE: Copy or move small version of statemachine here

#### Schedule

_Occasional repeating or one-off task manager_

Best-effort timing, but guaranteed to get to every task including those scheduled with busy

- `__iadd__`
    - Compute next time, insert into ordered list (behind existing tasks planned for the same time)
        - Maybe just compute next time and sort the list?  Have to check how sort breaks ties (I _think_ it preserves existing order, so add new task at the end)

- `__isub__`
    - del from list

- `__call__` (Often as the None or default handler)
    - Call busy (even if next task is ready, to guarantee progress)
    - When next task is ready, pop and re-add, call task if callable and emit (non-none?) result
    - Otherwise loop


#### Busy

_Simple round-robin cooperative multi-tasker_

Scheduler for ongoing high-frequency polling or background tasks

- docstrings
- testing

### Doneyard

#### Busy

- DONE: `__iadd__`
    - Append callable
- DONE: `__isub__`
    - Find callable
        - If index < i: i -= 1
    - Del callable
- PUNT: `clear_tasks`?
- DONE: `__call__(time limit)`
    - Round robin
        - If i >= len(tasks): i = 0
        - t = tasks[i], i += 1, call t, emit non-none results
        - loop until  # always do at least one task to ensure progress
    - When time limit expires
        - Emit None

---
