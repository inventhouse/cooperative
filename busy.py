# Busy: Copyright © 2021 Benjamin Holt - MIT License

import time

class Busy():
    def __init__(self, tasks=None, wait=time.sleep):
        self.tasks = list(tasks) if tasks is not None else []
        self.next_task = 0
        self.wait = wait  # REM: cut for _min


    def __call__(self, seconds=0):
        if not self.tasks:  # Wait if there's nothing to do  # REM: cut for _min
            assert seconds >= 0, "Task list is empty, refusing to sleep forever"  # REM: perhaps reconsider if CircuitPy user space ever gets access to interrupts
            return self.wait(seconds)
        end_t = time.monotonic() + seconds
        while True:  # Always advance by at least one task
            result = next(self)
            if result is not None:
                return result
            if seconds >= 0 and time.monotonic() >= end_t:
                return None


    def __next__(self):  # Fold into __call__ for _min
        t = self.tasks[self.next_task]
        self.next_task = (self.next_task + 1) % len(self.tasks)
        result = t() if callable(t) else t
        return result  # REM: keep result var to make folding-in above easy


    def __iadd__(self, task):  # Cut for _min
        self.tasks.append(task)
        return self


    def __isub__(self, task):  # Cut for _min
        # if task not in self.tasks:  # Be lenient about removing tasks that aren't scheduled
        #     return self
        i = self.tasks.index(task)  # Raises ValueError if task is not found
        if i < self.next_task:
            self.next_task -= 1
        del(self.tasks[i])
        if self.next_task >= len(self.tasks):
            self.next_task = 0
        return self


    def __len__(self):  # Cut for _min
        return len(self.tasks)


    def __iter__(self):  # Cut for _min
        return self
#####
